**What is this? How do i use it?** Type in a C type declaration and it will tell you how to read it. Exit the program with `>> exit`. Some illegal declarations may not be recognized as such (oops). Hope this tool can help :D

**How do i run it?**: clone or otherwise download the repository. navigate into the folder of the repository. run it with python (make sure you have python 3). So usually `python declaration.py` or `python3 declaration.py`. Then you can type in `>> int x` or any other declaration you'd like to have translated

**Example usage:**

```c
>> int (*owo())[]          
owo is a function () returning pointer to array of int
>> char *(*(**uwu[][8])())[]
uwu is array of array of 8 pointer to pointer to a function () returning pointer to array of pointer to char
```
