# worst code i ever wrote, pls don't judge :3
dbg = False

def a(s, i, j):
    global dbg
    desc = ''
    if dbg: print("right: ", end="")
    function_bracket_opened = 0
    while i < len(s):
        if dbg: print("\t\t",s[i])
        x = s[i]

        if x == ')':
            if function_bracket_opened > 0:
                function_bracket_opened -= 1
                desc += ') returning '
                i += 1
            break

        # () is a function declaration
        if x == '(':
            function_bracket_opened += 1
            desc += 'a function ('
        elif x == '[':
            desc += ' array of '
        elif x == ']':

            pass
        else:
            desc += x
        i = i + 1
    i = i + 1
    
    if dbg:
        print(desc)
        dr = len(desc)
        print("left: ",end="")
    # left
    desc += ' '
    while j >= 0 and s[j] not in ['(','[']:
        if dbg: print("\t\t",s[j])
        found = False

        if j >= 1:
            if s[j-1]+s[j] == '()':
                desc += 'function returning '
                found = True
            elif s[j-1]+s[j] == '[]':
                desc += ' array of '
                found = True
        if s[j] == '*':
            desc += ' pointer to '
            found = True
        if not found:
            # just say it
            kw = s[0:j+1].split()[-1]
            desc += kw+' '
            j -= len(kw)
        j -= 1
    j -= 1
    if dbg: print(desc[dr:])
    return (i,j,desc)

def check_legal(dec):
    # []()
    # ()()
    # ()[]

    # []()
    stack = []
    for l in dec:
        if l in '([':
            stack.append(l)
        elif l == ')':
            if len(stack)==0:
                return 'bracket\'s messed up'
            x = stack.pop()
            if x != '(':
                return 'brackets don\'t match'
        elif l == ']':    
            if len(stack)==0:
                return 'bracket\'s messed up'
            x = stack.pop()
            if x != '[':
                return 'brackets don\'t match'
    if len(stack) > 0:
        return 'unclosed brackets'
    if ']()' in dec:
        return 'can\'t have an array of functions'
    if '()()' in dec:
        return 'can\'t have a function that returns a function'
    if '()[' in dec:
        return 'can\'t have a function that returns an array'
    return '' 

TYPES_KW = ['char','unsigned','signed','int','short','long','double','float']
alphabet = 'abcdefghijklmnopqrstuvwxyz'

#s = input('int *x(int *, int)')
while True:

    identifier = ''

    s = input('>> ').strip()
    if s[len(s)-1] == ';':
        s = s[:-1]

    if s == 'exit':
        break

    ss = s.replace('char','').replace('unsigned','').replace('signed','').replace('int','').replace('short','').replace('long','').replace('double','').replace('float','')

    # poor persons regex
    found = False
    for l in ss:
        if l in alphabet or l in alphabet.upper():
            found = True
            # our identifier is this
            identifier += l
        elif found and l in '0123456789_-':
            identifier += l
        elif found:
            break
  
    s = s.replace('char','/char/').replace('unsigned','/unsigned/').replace('signed ','/signed/').replace('int','/int/').replace('short','/short/').replace('long','/long/').replace('double','/double/').replace('float','/float/')

    in_kw = False
    from_i = 0
    setFrom = False
    to_i = len(s)
    for i,l in enumerate(s):
        if l == '/':
            if in_kw:
                in_kw = False
            else:
                in_kw = True

        if not in_kw:
            if identifier == s[from_i:i]:
                to_i = i
            if identifier[0] == l and not setFrom:
                from_i = i
                setFrom = True

    #s = s.replace(identifier,'x')
    s = s[:from_i] + 'x' + s[to_i:]

    s = s.replace('/char/','char').replace('/unsigned/','unsigned').replace('/signed/','signed ').replace('/int/','int').replace('/short/','short').replace('/long/','long').replace('/double/','double').replace('/float/','float')

    legality = check_legal(s)
    if len(legality) > 0:
        # illegal
        print('ILLEGAL: '+legality)
        continue

    identifier_pos = s.index('x')

    d = 'x is '
    i = identifier_pos + 1
    j = identifier_pos - 1

    ls = len(s)
    if identifier_pos == len(s)-1:
        # edge case, identifier is last
        ls = len(s)+1

    while i < ls or j > 0:
        i,j,dd = a(s, i, j)
        d += dd
    print((' '.join([x.strip() for x in d.split()])).replace('x is',identifier+' is'))
